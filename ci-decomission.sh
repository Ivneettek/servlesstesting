#!/bin/bash

echo "Demolishing your awesome stacks.."
cd facility-api
serverless remove

cd ..
cd gateway
serverless remove

echo "Demolishing complete :)"
read
