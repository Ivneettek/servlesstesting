"use strict";
const _ = require('lodash')
const config = require('../config')

function successResponse(statusCode , responseBody , headers){
    const response = {
        statusCode: statusCode,
        headers: {
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*',
              'Access-Control-Allow-Credentials': "true"
        },
        body: responseBody
      };
 return response;     
}

function failResponse(statusCode , responseBody , headers){
    const response = {
        statusCode: statusCode,
        headers: {
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*',
              'Access-Control-Allow-Credentials': "true"
        },
        body: responseBody
      };
 return response;     
}
function validateToken(header){
    return true;
}

function populateTenant(header , body){
    //const mbody = _.merge({'system_pk':'#TENANT#tenant-id-000'} , body )
    let mbody = _.cloneDeepWith(body)
    mbody['system_pk'] = '#TENANT#tenant-id-000';
    console.log(`populateTenant ==> ${JSON.stringify(mbody)}`)
    return mbody
}
function findReplace(body , findkey , replaceVal){
    for (var attributename  in body) {
    //_.forEach(body, function(value, key) {
          //console.log(`key ===> ${key}`);
          if (attributename === findkey) {
           body[replaceVal] = '#FACILITY#'.concat(body[attributename])
           delete body[attributename]
           break
           }
          
        }
        console.log(`findReplace ===> ${body}`);
 return body;
}

function singleRequest(req){
    var sReq = {
        TableName : config.pipeline_configuration_tbl, 
        Item : req
    }
    return sReq;
   }



module.exports = { successResponse , failResponse , findReplace , singleRequest , validateToken , populateTenant}



