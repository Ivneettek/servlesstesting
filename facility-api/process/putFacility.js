"use strict";
const AWS = require("aws-sdk");
 var product = {
     put: async function(jsonstr) {
     console.log('Put Function -->'+JSON.stringify(jsonstr));
     var docClient = new AWS.DynamoDB.DocumentClient();
      let data = '';
      // Call DynamoDB to add the item to the table
      

      try {
      data = await docClient.put(jsonstr).promise();
      //data = await docClient.batchWrite(jsonstr).promise();
      }catch(ex ){
        console.log(` ==== < Inside error ${ex} <<====`)
      }
      
   
     return data;
}
 }

module.exports = product
