"use strict";
const xlsx = require('node-xlsx')
const fs = require('fs');
const AWS = require("aws-sdk");

  var FileExcelSheet = {
    read : async function(bucketname , filename) {
    /***
      * implementation of readFile 
    */
   const s3Config = {
    'Bucket': bucketname,
    'Key': filename
    }
    const s3 = new AWS.S3();
    const s3Obj = s3.getObject(s3Config).createReadStream();
    
    var buffers = [];
    for await (const chunk of s3Obj) {
      buffers.push(chunk);
    }
    var buffer = Buffer.concat(buffers);
    var workbook = xlsx.parse(buffer);
    return workbook;
  } , 
  sheet : async function(bucketname , name , sheetnameParams){
    var sheetdata =  {
      facility : {},
      address : {},
      document : {},
      setFacility : function(facilitySheet) {
        this.facility = facilitySheet
      },
      setAddress : function(addressSheet) {
        this.address = addressSheet
      },
      setDocument: function(docSheet) {
        this.document = docSheet
      }
    }
    var workbook =  await this.read(bucketname , name)
    console.log(" data return from read "+JSON.stringify(workbook))
    workbook.forEach(function(y) {
    console.log('Sheet Name ===>'+y.name)
    if (sheetnameParams.Facility === y.name){
      sheetdata.setFacility(y.data);
    } else if (sheetnameParams.Address === y.name){
      sheetdata.setAddress(y.data);
    } else if(sheetnameParams.Document === y.name){
      sheetdata.setDocument(y.data);
    }
    })
    return sheetdata;
  }
  
};

module.exports = FileExcelSheet;

