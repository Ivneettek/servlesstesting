"use strict";
const _ = require('lodash')


function prepareInsertObject(sheetData , tablename) {
  let data = [];
  let unionVals = {};
  if (_.isEmpty(sheetData.facility) && _.isEmpty(sheetData.address)){
    return {};
  }
  var Flabels = _.head(sheetData.facility)
  var Addlabels = _.head(sheetData.address)
  var Doclabels = {}
  if (!_.isEmpty(sheetData.document)) {
   Doclabels = _.head(sheetData.document)
  }
   _.each(sheetData.facility, function (Fjobj, index) {
      if (index > 0) {
        var zipObj = getFacilityZipObject(Flabels, Fjobj);
        var addZipObj = {};
        var docZipObj = {};
        _.each(_.filter(sheetData.address,_.replace(zipObj.system_sk,'#FACILITY#','')) , function(addjobj, index){
          addZipObj = getAddlZipObject(Addlabels , addjobj)
      });
      if (!_.isEmpty(sheetData.document)) {
      _.each(_.filter(sheetData.document,_.replace(zipObj.system_sk,'#FACILITY#','')) , function(docjobj, index){
        docZipObj = getAddlZipObject(Addlabels , docjobj)
     });
   }
   if (!_.isEmpty(zipObj)){
     if (!_.isEmpty(addZipObj)){
      if (!_.isEmpty(docZipObj)){
        unionVals = _.union(zipObj , addZipObj, docZipObj)
      }else {
        unionVals = _.union(zipObj , addZipObj)
      }
   }else {
    unionVals = zipObj ;
   }
  }else {
    return {};
  }
    
   console.log('*************************')   
   console.log(JSON.stringify(zipObj)) 
   console.log(JSON.stringify(addZipObj))  
   console.log(JSON.stringify(docZipObj))      
   console.log('*************************')     
   data.push(
          {
              TableName :'os_pipeline_configuration' , 
              Item : unionVals
          }
          
          );
      }
    });
  return data;
}

function getFacilityZipObject(labels, jobj) {
  let tempLabels = [...labels];
  // Replacing the labels with DynamoDB design
  findReplaceStr(tempLabels, 'FACILITY_CODE', 'system_sk');
  findReplaceStr(tempLabels, 'FACILITY_NAME', 'secondary_idx_key_1');
  let returnJobj = appendString(jobj, '#FACILITY#')
  tempLabels.unshift('system_pk')
  returnJobj.unshift('#TENANT#tenant-id-000')
  var zipObj = _.zipObject(tempLabels, returnJobj)
  return zipObj;
}

function getAddlZipObject(labels, jobj) {
  let tempLabels = [...labels];
  // Replacing the labels with DynamoDB design
  let returnJobj = appendString(jobj, '#FACILITY#')
  var zipObj = _.zipObject(tempLabels, returnJobj)
  return zipObj;
}

function findReplaceStr(stringArr, findstr, replacestr) {
  var index = _.findIndex(stringArr, function (str) {
    return str === findstr;

  });

  console.log(`index ${JSON.stringify(stringArr)} ---- ${findstr} -- ${index}`)
  stringArr.splice(index, 1, replacestr);
  
}
function appendString(stringArr, appendStr) {
  let newArray = [];
  _.forEach(stringArr, function (str) {

    newArray.push((str != null) ? appendStr.concat(str) : '')
  })
  return newArray;
}

module.exports = { prepareInsertObject }



